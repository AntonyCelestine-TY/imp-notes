package com.te.sortanarray;

import java.util.Arrays;

public class UsermainCode {
	
	public static void sortArray(int [] arr) {
		
		Arrays.stream(arr).boxed().sorted().forEach(collect->System.out.println("ascending order:"+collect));;
		
		Arrays.stream(arr).boxed().sorted((o1, o2) -> o2.hashCode() - o1.hashCode())
				.forEach(l->System.out.println("decending order:"+l));
		
	}

}
