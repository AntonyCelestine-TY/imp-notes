package com.te.removeduplicate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UserMainCode {
	public static List<Integer> removeDupliateNumber(int [] arr) {
		return Arrays.stream(arr).distinct().boxed().collect(Collectors.toList());
	}

}
