package com.te.sortstring;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter your size of an array of String:");
		Integer size = scanner.nextInt();
		String[] arr = new String[size];
		for (int i = 0; i <arr.length; i++) {
			arr[i] = scanner.nextLine();

		}
		UserMainCode.sortArrayOfString(arr);
	}

}
