package com.te.findmin;

import java.util.Arrays;

public class UserMainCode {
	public static Integer findMinNumber(int [] arr) {
		return Arrays.stream(arr).max().getAsInt();
	}
}
