package com.te.findmax;

import java.util.Arrays;

public class userMainCode {

	public static Integer findMaxNumber(int [] arr) {
		return Arrays.stream(arr).max().getAsInt();
	}
}
