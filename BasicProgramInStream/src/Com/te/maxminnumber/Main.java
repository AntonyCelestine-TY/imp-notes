package Com.te.maxminnumber;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter your size of an list:");
		Integer size = scanner.nextInt();
		List<Integer> list= new ArrayList<Integer>();
		for (int i = 0; i < size; i++) {
			list.add(scanner.nextInt());

		}
		UserMainCode.maxAndMinNumber(list);
	}

}
