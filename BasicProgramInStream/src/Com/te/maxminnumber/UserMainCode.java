package Com.te.maxminnumber;

import java.util.List;

public class UserMainCode {
	public static void maxAndMinNumber(List<Integer> list) {
		Integer n = list.stream().max(Integer::compare).get();
		System.out.println("Max:" + n);
		System.out.println("****************");
		Integer n2 = list.stream().min(Integer::compare).get();
		System.out.println("Min:" + n2);
	}
		

}
