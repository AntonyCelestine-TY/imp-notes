package Com.te.printthenumberstartwith;

import java.util.List;
import java.util.Scanner;

public class UserMainCode {
	
	public static void numberStartWith( List<Integer> list) {
		
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter your number start with:");
		String nextInt = scan.next();
		
		list.stream().map(t-> t +" ").filter(t-> t.startsWith(nextInt)).forEach(n-> System.out.println("Number are:"+ n+" "));
		
	}

}
