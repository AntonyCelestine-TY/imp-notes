package Com.te.primenumber;

import java.util.stream.IntStream;

public class UserMainCode {
	
	public static void prime(Integer from, Integer to) {
		
		IntStream.rangeClosed(from, to).filter(t->{
			Boolean flag=true;
			for (int i = 2; i <=t/i; i++) {
				if(t%i==0) {
					flag=false;
				}
			}
			return flag;
		}).forEach(t-> System.out.println(t));
		
	}

}
