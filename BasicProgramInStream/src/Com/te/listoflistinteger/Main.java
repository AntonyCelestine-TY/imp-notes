package Com.te.listoflistinteger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<List<List<String>>> singletonList = Collections.singletonList(Arrays.asList(Arrays.asList("a", "s", "a")));
		System.out.println("Lists:"+singletonList);
		List<List<String>> collect1 = singletonList.stream().flatMap(List::stream).collect(Collectors.toList());
		System.out.println("Double List:"+collect1);
		List<String> collect2 = collect1.stream().flatMap(List::stream).collect(Collectors.toList());
		System.out.println("Single List:" + collect2);
	}

}
