package Com.te.stringreverse;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserMainCode {

	public static String reverse(String str) {
		
		String collect = Stream.of(str).map(t-> new StringBuffer(t).reverse()).collect(Collectors.joining());
		return collect;
	}
}
