package Com.te.stringtointeger;

import java.util.List;
import java.util.stream.Collectors;

public class UserMainCode {
	
	public static List<Integer> convertStringToInteger(List<String> list) {
		return list.stream().map(Integer::parseInt).collect(Collectors.toList());
	}

}
